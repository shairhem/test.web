import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NpvRequest } from '../models/npv-request.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NpvService {

  constructor(private http: HttpClient) { }

  public computeNpv(request: NpvRequest): Observable<any> {
    return this.http.post(`${environment.baseUrl}npv`, request);
  }
}
