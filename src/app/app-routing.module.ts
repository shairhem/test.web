import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NpvComponent } from './components/npv/npv.component';

const routes: Routes = [
  {
    path: '',
    component: NpvComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
