import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NpvService } from 'src/app/services/npv.service';
import { NpvRequest } from 'src/app/models/npv-request.model';
import { isPositiveNumber } from 'src/app/validators/positive-number.validator';
import { validateUpperBoundLimit } from 'src/app/validators/upper-bound-limit.validator';


@Component({
  selector: 'app-npv',
  templateUrl: './npv.component.html',
  styleUrls: ['./npv.component.scss']
})
export class NpvComponent implements OnInit {

  npvForm: FormGroup;
  isLoading: boolean = false;
  computedValues = null;

  constructor(
    private formBuilder: FormBuilder,
    private npvService: NpvService,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  compute(): void {
    this.computedValues = null;
    const request: NpvRequest = this.npvForm.value;
    this.isLoading = true;
    this.npvService.computeNpv(request).subscribe((response) => {
      this.computedValues = response;
      this.isLoading = false
    }, () => {
      this.isLoading = false;
    })
  }

  addCashFlow(): void {

    console.log(this.npvForm);
    const controls: FormArray = <FormArray>this.npvForm.controls['cashFlows'];
    const newControl: FormGroup = this.formBuilder.group({
      'value': '',
    });
    controls.push(newControl);
  }

  removeCashFlow(index: number): void {
    const controls: FormArray = <FormArray>this.npvForm.controls['cashFlows'];
    controls.removeAt(index);
  }

  private initForm(): void {
    this.npvForm = this.formBuilder.group({
      initialInvestment: ['', [Validators.required, isPositiveNumber]],
      lowerBound: ['', [Validators.required, isPositiveNumber]],
      upperBound: ['', [Validators.required, isPositiveNumber]],
      increment: ['', [Validators.required, isPositiveNumber]],
      cashFlows: this.formBuilder.array([], Validators.compose([Validators.required])),
    }, {
      validator: validateUpperBoundLimit
    });
    
    this.addCashFlow();
  }
}
