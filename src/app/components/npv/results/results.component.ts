import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  @Input() computedValues: any;
  
  displayedColumns: string[] = ['rate', 'npv'];
  
  dataSource = [];
  isReady = false;
  
  constructor() { }

  ngOnInit() {
    this.isReady = false;
    this.dataSource = this.computedValues;
    this.isReady = true;
  }
}
