import { AbstractControl } from '@angular/forms';

export function isPositiveNumber(control: AbstractControl) {
    let result: object = null;
    const value: number = control.value

    if (value <= 0) {
        result = { positiveNumber : true };
    }

    return result;
}