import { FormGroup } from '@angular/forms';

export function validateUpperBoundLimit(formGroup: FormGroup) {
    let result: object = null;
    const upperBound = formGroup.controls['upperBound'];
    const lowerBoundValue: number = formGroup.controls['lowerBound'].value;
    const upperBoundValue: number = upperBound.value;
    
    // reset the errors of upperBound control
    upperBound.setErrors(null);

    if (lowerBoundValue >= upperBoundValue) {
        result = { upperBound: true };
        upperBound.setErrors(result);
    }

    return result;
}