export class NpvRequest {
    lowerBound: number;
    upperBound: number;
    interest: number;
    cashFlowId: number;
}