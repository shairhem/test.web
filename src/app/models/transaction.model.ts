export class Transaction {
    id: number;
    cashFlowId: number;
    value: number;
}